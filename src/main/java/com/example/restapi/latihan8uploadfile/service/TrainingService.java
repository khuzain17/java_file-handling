package com.example.restapi.latihan8uploadfile.service;

import com.example.restapi.latihan8uploadfile.entity.Training;

import java.util.Map;

public interface TrainingService {
    public Map insert(Training training);

    public Map update(Training training);

    public Map delete(Long idTraining);

    public Map getById(Long idTraining);

    public Map getAll();

}
