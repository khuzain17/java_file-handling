package com.example.restapi.latihan8uploadfile.service;

import com.example.restapi.latihan8uploadfile.entity.EmployeeDetail;

import java.util.Map;

public interface EmployeeDetailService {
    public Map insert(EmployeeDetail employeeDetail);

    public Map update(EmployeeDetail employeeDetail);

    public Map delete(Long idEmployeeDetail);

    public Map getById(Long idEmployeeDetail);

    public Map getAll();
}
