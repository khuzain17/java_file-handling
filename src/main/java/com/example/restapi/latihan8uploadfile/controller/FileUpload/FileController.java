package com.example.restapi.latihan8uploadfile.controller.FileUpload;

import com.example.restapi.latihan8uploadfile.entity.Employee;
import com.example.restapi.latihan8uploadfile.repository.EmployeeRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Value("${app.uploadto.cdn}")
    private String UPLOADED_FOLDER;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private EmployeeRepo employeeRepo;

    @RequestMapping(value = "/synrgy/upload", method = RequestMethod.POST, consumes = {"multipart/form-data", "application/json"})
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id) throws IOException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
        String strDate = formatter.format(date);

        String fileName = UPLOADED_FOLDER + strDate + file.getOriginalFilename();
        String fileNameforDOwnload = strDate + file.getOriginalFilename();
        Path TO = Paths.get(fileName);

        if (!((file.getContentType().equals("image/png")) || (file.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")))) {
            return new UploadFileResponse(file.getOriginalFilename(), null, file.getContentType(), file.getSize(), "File is not a .png or .docx file");
        }

        try {
            Files.copy(file.getInputStream(), TO);

            //here you set file Name
            Employee obj = employeeRepo.getById(id);
            obj.setFilename(new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date())+
                    file.getOriginalFilename());
            employeeRepo.save(obj);

        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new UploadFileResponse(fileName, null,
                    file.getContentType(), file.getSize(), e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/synrgy/showFile/")
                .path(fileNameforDOwnload)
                .toUriString();

        return new UploadFileResponse(fileNameforDOwnload, fileDownloadUri,
                file.getContentType(), file.getSize(), "false");
    }

    @GetMapping("synrgy/showFile/{fileName:.+}")
    public ResponseEntity<Resource> showFile(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        String contentType = null;
        try {

            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PostMapping("synrgy/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) throws IOException {
        return Arrays.asList(files)
                .stream()
                .map(file -> {
                    try {
                        // step 1 : call method uplaod
                        return uploadFile(file, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/synrgy/update", method = RequestMethod.PUT, consumes = {"multipart/form-data", "application/json"})
    public UploadFileResponse updateFile(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id) throws IOException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
        String strDate = formatter.format(date);

        String fileName = UPLOADED_FOLDER + strDate + file.getOriginalFilename();
        String fileNameforDOwnload = strDate + file.getOriginalFilename();
        Path TO = Paths.get(fileName);

        Employee obj = employeeRepo.getById(id);

        if (!((file.getContentType().equals("image/png")) || (file.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")))) {
            return new UploadFileResponse(file.getOriginalFilename(), null, file.getContentType(), file.getSize(), "File is not a .png or .docx file");
        }

        if (obj == null) {
            return new UploadFileResponse(file.getOriginalFilename(), null, file.getContentType(), file.getSize(), "Id Invalid");
        }

        try {
            Files.copy(file.getInputStream(), TO);
            //here you set file Name
            obj.setFilename(new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date())+
                    file.getOriginalFilename());
            employeeRepo.save(obj);

        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new UploadFileResponse(fileName, null,
                    file.getContentType(), file.getSize(), e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/synrgy/showFile/")
                .path(fileNameforDOwnload)
                .toUriString();

        return new UploadFileResponse(fileNameforDOwnload, fileDownloadUri,
                file.getContentType(), file.getSize(), "false");
    }

//    @GetMapping("synrgy/list-file")
//    public ResponseEntity<Resource> showFile(HttpServletRequest request) {
//        Resource resource = fileStorageService.loadFileAsResource(fileName);
//
//        String contentType = null;
//        try {
//
//            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
//
//        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
//        }
//
//        if (contentType == null) {
//            contentType = "application/octet-stream";
//        }
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(contentType))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
//                .body(resource);
//    }



    private File multipartToFile(MultipartFile upload, String routeName) {
        String base = "";

        logger.info(String.format("Trying upload file: %s", upload.getOriginalFilename()));

        File file = new File(base + upload.getOriginalFilename());

        try {
            logger.info(String.format("Saving uploaded file to: '%s'", file.getAbsolutePath()));
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(upload.getBytes());
            fos.close();
        } catch (IOException e) {
            logger.error(String.format("Error: POST|UPLOAD %s", routeName), e);
        }

        return file;
    }
    private File multipartToFile(MultipartFile upload) {
        return multipartToFile(upload, UPLOADED_FOLDER);
    }
}
